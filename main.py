from re import search
from DUI import *
from random import randint
import sys
from time import sleep
import webbrowser

def guess():
	an = randint(0,10)
	frame.showWindow(1)
	while True:
		txt = listen.getText()
		try:
			res = int(txt)
		except:
			continue
		if res == an:
			# 猜对
			gameWindow.updateWidget(0, Alert())
			frame.addWindow(gameWindow, 1)
			frame.showWindow(2)
			frame.listener.run()
			return
		else:
			# 猜错
			gameWindow.updateWidget(0, Alert("猜错了！不是"+str(res)))
			frame.addWindow(gameWindow, 1)
			frame.showWindow(1)
def load():
    loadwindow.updateWidget(3,Text())
    loadwindow.updateWidget(4,Text())
    loadwindow.updateWidget(5,Text())
    frame.showWindow(3)
    for i in range(1,11):
        loadwindow.updateWidget(3,Text("--"*i))
        loadwindow.updateWidget(4,Text("=="*i + ">"))
        loadwindow.updateWidget(5,Text(str(10*i)+"%"))
        sleep(0.5)
        frame.showWindow(3)
    sleep(0.5)
    mainW()
        
def mainW():
    frame.showWindow(0)
def pic():
    frame.showWindow(4)
    frame.listener.run()
def show_calculator():
    frame.showWindow(5)
    line = 3
    while True:
        txt = listen.getText()
        if txt == "q":
            global calculator
            calculator = Window("计算器",width=50)
            calculator.addWidget(2,Text("键入q返回主界面,wq不清除记录退出"))
            frame.addWindow(calculator,5)
            break
        elif txt == "wq":
            break
        else:
            try:
                answer = txt + "=" + str(eval(txt))
            except:
                if "=" in txt:
                    answer = "'" + txt + "'" + "中含有\'=\',不需要输入\'=\'"
                else:
                    answer = "'" + txt + "'" + "错误"
            finally:
                calculator.addWidget(line,Text(answer))
                frame.showWindow(5)
                line += 1

    mainW()
def fastsearch():
    frame.showWindow(6)
    while True:
        txt = listen.getText()
        if txt == "q":
            break
        else:
            try:
                webbrowser.open(f"https://www.baidu.com/s?wd={txt}", new=0, autoraise=True)
                webbrowser.open_new(f"https://cn.bing.com/search?q={txt}")
                webbrowser.open_new(f"https://www.google.com/search?q={txt}")
            except:
                print("错误")
    mainW()

frame = Frame()

mainWindow = Window("主界面")
mainWindow.addWidget(2, Text("DUI工具箱", 1))
mainWindow.addWidget(5, Button("猜数字游戏", onClick=guess))
mainWindow.addWidget(6, Button("加载动画", onClick=load))
mainWindow.addWidget(7, Button("字符画", onClick=pic))
mainWindow.addWidget(8, Button("计算器", onClick=show_calculator))
mainWindow.addWidget(9, Button("快搜",onClick=fastsearch))
mainWindow.addWidget(10, Button("退出", onClick=sys.exit))
frame.addWindow(mainWindow, 0)

gameWindow = Window("猜数界面")
gameWindow.addWidget(0, Alert())
gameWindow.addWidget(2, Text("猜猜随机出来的是几(0-10)"))
gameWindow.addWidget(4, Text())
frame.addWindow(gameWindow, 1)

overWindow = Window("猜数成功!")
overWindow.addWidget(2, Text("恭喜你猜对了！", 1))
overWindow.addWidget(5, Button("返回主界面", onClick=mainW))
frame.addWindow(overWindow, 2)

loadwindow = Window("加载界面")
loadwindow.addWidget(2,Text("加载完毕后会自动退出"))
loadwindow.addWidget(3,Text())
loadwindow.addWidget(4,Text())
loadwindow.addWidget(5,Text())
frame.addWindow(loadwindow,3)

picture = Window("字符画")
picture.addWidget(2,Text("DDD   U   U IIIII"))
picture.addWidget(3,Text("D  D  U   U   I"))
picture.addWidget(4,Text("D   D U   U   I"))
picture.addWidget(5,Text("D  D  U   U   I"))
picture.addWidget(6,Text("DDD    UUU  IIIII"))
picture.addWidget(7,Button("回车键返回主界面", onClick=mainW))
frame.addWindow(picture,4)

calculator = Window("计算器",width=50)
calculator.addWidget(2,Text("键入q返回主界面,wq不清除记录退出"))
frame.addWindow(calculator,5)

search = Window("快搜")
search.addWidget(2,Text("键入q返回主界面"))
search.addWidget(3,Text("请输入要搜索的关键词"))
frame.addWindow(search,6)

listen = Listener(0)
dic = \
	{
		"q": sys.exit,
		"e": sys.exit,
		"w": mainWindow.up,
		"s": mainWindow.down,
		"y": listen.confirm,
		"\n": None
	}
listen.setDict(dic)
frame.setListener(listen)

while True:
	frame.showWindow(0)
	frame.listener.run()
